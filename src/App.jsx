import React from 'react'
import { observer, inject } from 'mobx-react'

import { RouterComponent } from '@routers'

@inject(({ stores }) => stores)
@observer
class App extends React.PureComponent {
  constructor(props) {
    super(props)
    this.updateDimensions()
  }

  updateDimensions = () => {
    const isMobile = window.innerWidth <= 1024
    if (this.props.globalState.isMobile !== isMobile) {
      this.props.globalState.setIsMobile(window.innerWidth <= 1024)
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
  }

  render() {
    return (
      <div className='app-container'>
        <RouterComponent />
      </div>
    )
  }
}

export default App
