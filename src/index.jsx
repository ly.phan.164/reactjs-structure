import React from 'react'
import { render } from 'react-dom'

import './index.less'

// Apollo Graphql
import { ApolloProvider } from '@apollo/react-hooks'
import { client } from '@graphql'

// Mobx
import { Provider } from 'mobx-react'
import { stores } from '@stores'

// i18n
import { I18nextProvider } from 'react-i18next'
import i18n from './languages'

import App from './App'
import * as serviceWorker from './serviceWorker'

render(
  <ApolloProvider client={client}>
    <Provider stores={stores}>
      <I18nextProvider i18n={i18n}>
        {/* <React.StrictMode> */}
        <App />
        {/* </React.StrictMode> */}
      </I18nextProvider>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root')
)

serviceWorker.register()
