import { observable, action } from 'mobx'
import { appConstants } from '@constants'

export class GlobalState {
  @observable
  isAuth = !!window.localStorage.getItem(appConstants.userTokenKey)

  @observable
  isMobile = false

  @observable
  userId = null

  @observable
  myInfo = null

  @observable
  session = null

  @action
  onAuth = token => {
    window.localStorage.setItem(appConstants.userTokenKey, token)
    this.isAuth = true
  }

  @action
  setIsMobile = isMobile => {
    this.isMobile = isMobile
  }

  @action
  onEnterSession = ({ userId, session }) => {
    this.isAuth = true
    this.userId = userId
    this.session = session
  }

  @action
  setMyInfo = myInfo => {
    this.myInfo = myInfo
  }

  @action
  onLeaveSession = () => {
    this.isAuth = false
    this.userId = null
    this.session = null
    window.localStorage.clear()
  }
}
