const routesPath = {
  root: '/',
  notfound: '/notfound',

  free: '/free'
}

const noAuthenRoutes = [
  {
    path: routesPath.free,
    exact: true,
    component: 'free'
  }
]

const authenRoutes = [
  {
    path: routesPath.root,
    exact: true,
    component: 'root'
  }
]

const freeRoutes = [
  {
    path: routesPath.free,
    exact: true,
    component: 'free'
  }
]

export { routesPath, noAuthenRoutes, authenRoutes, freeRoutes }
