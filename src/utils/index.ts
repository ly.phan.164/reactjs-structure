/** Friendly errors, catch all Queries/Mutations errors in catch block,
 * log error to console only in dev mode and show notification
 * in production mode
 *
 * - `errorKey`: queries/mutations name
 * - `error`: error that log to console in dev mode
 * - `errorMessage`: message that show in production mode
 */
export function returnError(errorKey: string, { error, errorMessage }, otherSettings?: { [key: string]: any }) {
  if (process.env.NODE_ENV === 'development') {
    console.error(`${errorKey} ERROR:`, error)
  }

  // *: Replace by your own notification component
  return console.log()
}
