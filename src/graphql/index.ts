import { ApolloClient } from 'apollo-client'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { ApolloLink, split } from 'apollo-link'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { onError } from 'apollo-link-error'
import { createUploadLink } from 'apollo-upload-client'

import { appConstants } from '@constants'

// Authentication
const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers = {}, localToken = localStorage.getItem(appConstants.userTokenKey) }) => {
    if (localToken) {
      headers[appConstants.userTokenKey] = localToken
    }
    return {
      headers
    }
  })

  return forward(operation)
})

// onError
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.forEach(({ message, locations, path }) =>
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
    )
  if (networkError) console.log(`[Network error]: ${networkError}`)
})

// Apollo Server connect
const URN = process.env.REACT_APP_GRAPHQL_URN || `${window.location.host}/${process.env.REACT_APP_END_POINT}`

const httpLink = createUploadLink({
  uri: `${window.location.protocol}//${URN}`
})

const wsLink = new WebSocketLink({
  uri: `${window.location.protocol === 'https:' ? 'wss:' : 'ws:'}//${URN}`,
  options: {
    reconnect: true,
    timeout: 30000,
    connectionParams: () => ({
      'access-token': window.localStorage.getItem('access-token') || null
    })
  }
})

const terminatingLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription'
  },
  wsLink,
  httpLink
)

// Cache
const cache = new InMemoryCache({
  addTypename: false
})

const client = new ApolloClient({
  link: ApolloLink.from([authLink, errorLink, terminatingLink]),
  cache
})

export { default as gql } from 'graphql-tag'
export { client as default, client }
export * from './QMStrings'
